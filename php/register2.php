<?php
	session_start();
	require_once("config.php");
	
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	use PHPMailer\PHPMailer\SMTP;
	require '../PHPMailer-master/src/Exception.php';
	require '../PHPMailer-master/src/PHPMailer.php';
	require '../PHPMailer-master/src/SMTP.php';
	
	// Get web data in
	$FirstName = $_GET["FirstName"];
	$LastName = $_GET["LastName"];
	$Email = $_GET["Email"];
	
	print "Web data arrived ($FirstName, $LastName, $Email) <br>";
	// Insert into the database
	// The macros come from config.php
	$con = mysqli_connect(SERVER, USER, PASSWORD, DATABASE);
	if (!$con)
	{
		$_SESSION["RegState"] = -1;
		$_SESSION["Message"] = "Database connection failure: ".mysqli_error($con);
		header("location:../index.php");
		exit();
	}
	print "Database connected <br>";
	// Generate the Acode
	$Acode = rand();
	// Build the query
	$query = "INSERT into Visitors (FirstName, LastName, Email, Acode) values ('$FirstName', '$LastName', '$Email', '$Acode');";
	$result = mysqli_query($con, $query);
	if (!$result)
	{
		$_SESSION['RegState'] = -2;
		$_SESSION['Message'] = "Register2 insert query failed: ".mysqli_error($con);
		header("location:../index.php");
		exit();
	}

	// Registeration successful
	// Build authentication email
	$Message = "Please click the link to complete registration: http://cis-linux2.temple.edu/~tuc55611/final/php/authenticate.php?Email=$Email&Acode=$Acode";
	print "Email built ($Message) <br>";
	
	// Build the PHPMailer object:
	$mail= new PHPMailer(true);
	try { 
		$mail->SMTPDebug = 2; // Wants to see all errors
		$mail->IsSMTP();
		$mail->Host="smtp.gmail.com";
		$mail->SMTPAuth=true;
		$mail->Username="cis105223053238@gmail.com";
		$mail->Password = 'g+N3NmtkZWe]m8"M';
		$mail->SMTPSecure = "ssl";
		$mail->Port=465;
		$mail->SMTPKeepAlive = true;
		$mail->Mailer = "smtp";
		$mail->setFrom("tuc55611@temple.edu", "Saurav Guru");
		$mail->addReplyTo("tuc55611@temple.edu","Saurav Guru");
		$mail->addAddress($Email,"$FirstName $LastName");
		$mail->Subject = "Welcome";
		$mail->Body = $Message;
		$mail->send();
		print "Email sent ... <br>";
		$_SESSION["RegState"] = 0; // To force the login view, it is set to 0
		$_SESSION["Message"] = "Registration Success. Email sent.";
		header("location:../index.php");
		exit();
	} catch (phpmailerException $e) {
		$_SESSION["Message"] = "Mailer error: ".$e->errorMessage();
		$_SESSION["RegState"] = -4;
		print "Mail send failed: ".$e->errorMessage;	
		header("location:../index.php");
		exit();
	}

?>