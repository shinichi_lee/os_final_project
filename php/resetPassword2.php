<?php
	session_start();
	require_once("config.php");
	
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	use PHPMailer\PHPMailer\SMTP;
	require '../PHPMailer-master/src/Exception.php';
	require '../PHPMailer-master/src/PHPMailer.php';
	require '../PHPMailer-master/src/SMTP.php';
	
	// Get the web data
	$Email = $_GET["Email"];
	
	// Coonect to mysql
	$con = mysqli_connect(SERVER, USER, PASSWORD, DATABASE);
	// Build the query to check if email exists in the database
	$query = "Select * from Visitors where Email='$Email';";
	// Send the query
	$result = mysqli_query($con, $query);
	
	// Check the query result
	if ($result)
	{
		// Get the number of rows that were returned from the query, there should only be 1 match
		$rows = mysqli_num_rows($result);
		// If there was one match then the email exists
		if ($rows == 1)
		{
			// Generate the Acode
			$Acode = rand();
			// Build the query
			$query = "Update Visitors set Acode='$Acode' where Email='$Email';";
			$result = mysqli_query($con, $query);
			if (!$result)
			{
				$_SESSION['RegState'] = -2;
				$_SESSION['Message'] = "resetPassword2 update query failed: ".mysqli_error($con);
				header("location:../index.php");
				exit();
			}

			// Build password reset email
			$Message = "Please click the link to create a new password: http://cis-linux2.temple.edu/~tuc55611/final/php/resetPassword3.php?Email=$Email&Acode=$Acode";
			
			// Build the PHPMailer object:
			$mail= new PHPMailer(true);
			try { 
				$mail->SMTPDebug = 2; // Wants to see all errors
				$mail->IsSMTP();
				$mail->Host="smtp.gmail.com";
				$mail->SMTPAuth=true;
				$mail->Username="cis105223053238@gmail.com";
				$mail->Password = 'g+N3NmtkZWe]m8"M';
				$mail->SMTPSecure = "ssl";
				$mail->Port=465;
				$mail->SMTPKeepAlive = true;
				$mail->Mailer = "smtp";
				$mail->setFrom("tuc55611@temple.edu", "Saurav Guru");
				$mail->addReplyTo("tuc55611@temple.edu","Saurav Guru");
				$mail->addAddress($Email,"Password Recovery");
				$mail->Subject = "Password Reset";
				$mail->Body = $Message;
				$mail->send();
				$_SESSION["RegState"] = 0; // To force the login view, it is set to 0
				$_SESSION["Message"] = "Password reset link sent to the email";
				header("location:../index.php");
				exit();
			} catch (phpmailerException $e) {
				$_SESSION["Message"] = "Mailer error: ".$e->errorMessage();
				$_SESSION["RegState"] = -4;
				header("location:../index.php");
				exit();
			}
		}
		// Otherwise the email does not exist
		else
		{
			$_SESSION["RegState"] = 6;
			$_SESSION["Message"] = "Email not found";
			header("location:../index.php");
			exit();
		}
	}
	// Database access failure
	else
	{
		$_SESSION["RegState"] = -7;
		$_SESSION["Message"] = "Database access failure: ".mysqli_error($con);
		header("location:../index.php");
		exit();
	}
?>