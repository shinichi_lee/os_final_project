<?php
	session_start();
	require_once("config.php");
	
	// Get the web data
	$Email = $_GET["Email"];
	$Password = $_GET["Password"];
	$ConfirmPassword = $_GET["ConfirmPassword"];
	
	// Check if the passwords do not match
	if ($Password != $ConfirmPassword)
	{
		// Stay on the same view
		$_SESSION["RegState"] = 2;
		$_SESSION["Message"] = "The passwords do not match.";
		header("location:../index.php");
		exit();
	}
	else
	{
		// Get the hash of password
		$Password = md5($Password);
		
		// Coonect to mysql
		$con = mysqli_connect(SERVER, USER, PASSWORD, DATABASE);
		// Build the query to get Email and Acode from the database
		$query = "Update Visitors set Password='$Password' where Email='$Email';";
		// Send the query
		$result = mysqli_query($con, $query);
		
		// Check the query result
		if ($result)
		{
			// Password saved
			// Go back to the login view
			$_SESSION["RegState"] = 0;
			$_SESSION["Message"] = "Password set successfully";
			header("location:../index.php");
		}
		// Database access failure
		else
		{
			$_SESSION["RegState"] = -5;
			$_SESSION["Message"] = "Database access failure: ".mysqli_error($con);
			header("location:../index.php");
		}
	}
	exit();
?>