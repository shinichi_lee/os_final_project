<?php
	session_start();
	require_once("config.php");
	
	// Get web data
	$Email = $_GET["Email"];
	$Acode = $_GET["Acode"];
	
	// Coonect to mysql
	$con = mysqli_connect(SERVER, USER, PASSWORD, DATABASE);
	// Build the query to get Email and Acode from the database
	$query = "Select * from Visitors where Email='$Email' and Acode='$Acode';";
	// Send the query
	$result = mysqli_query($con, $query);
	
	// Check the query result
	if ($result)
	{
		// Get the number of rows that were returned from the query, there should only be 1 match
		$rows = mysqli_num_rows($result);
		// If there was one match then the authentication passed
		if ($rows == 1)
		{
			$_SESSION["RegState"] = 2;
			$_SESSION["Email"] = $Email;
			header("location:../index.php");
			exit();
		}
		// Otherwise there was a failure
		else
		{
			$_SESSION["RegState"] = -3;
			$_SESSION["Message"] = "Authentication Failed: ".mysqli_error($con);
			header("location:../index.php");
			exit();
		}
	}
	// Database access failure
	else
	{
		$_SESSION["RegState"] = -4;
		$_SESSION["Message"] = "Database access failure: ".mysqli_error($con);
		header("location:../index.php");
	}
	exit();
?>