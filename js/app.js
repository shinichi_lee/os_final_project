$(document).ready(function(){
	$.ajax({
		url: "http://localhost/chartjs/data.php",
		method: "GET",
		success: function(data) {
			console.log(data);
			var ElapsedTime = [];
			var MFLOPS = [];

			for(var i in data) {
				player.push("ElapsedTime " + data[i].ElapsedTime);
				score.push(data[i].MFLOPS);
			}

			var chartdata = {
				labels: ElapsedTime,
				datasets : [
					{
						label: 'ElapsedTime MFLOPS',
						backgroundColor: 'rgba(200, 200, 200, 0.75)',
						borderColor: 'rgba(200, 200, 200, 0.75)',
						hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
						hoverBorderColor: 'rgba(200, 200, 200, 1)',
						data: score
					}
				]
			};

			var ctx = $("#mycanvas");

			var barGraph = new Chart(ctx, {
				type: 'bar',//pie,graph etc..,
				data: chartdata
			});
		},
		error: function(data) {
			console.log(data);
		}
	});
});